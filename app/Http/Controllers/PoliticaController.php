<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Politica;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response as FacadeResponse;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PoliticaController extends Controller
{
    public function register(Request $request)
    {

        //Recoger datos del usuario por post
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);

        if (!empty($params) && !empty($params_array)) {
            //Validar datos
            $validate = Validator::make($params_array, [
                'nombre' => 'required',
                'archivo' => 'required'
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'No se ha enviado el formulario correctamente',
                    'errors' => $validate->errors()
                );
            } else {
                //validacion correcta
                //crear el genero
                $politica = new Politica();
                $politica->nombre = $params_array['nombre'];
                $politica->archivo = $params_array['archivo'];
                //guardar usuario
                $politica->save();
                $data = array(
                    'status' => 'succes',
                    'code' => 200,
                    'message' => 'El formulario ha sido enviado',
                    'politica' => $politica
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'Los datos enviados no son correctos',
            );
        }

        return response()->json($data, $data['code']);
    }

    public function upload(Request $request)
    {
        //recoger datos de la peticion
        $file = $request->file('file0');

        //guardar file 
        if ($file) {
            $file_name = time() . $file->getClientOriginalName();
            Storage::disk('politicas')->put($file_name, File::get($file));

            $data = array(
                'code' => 200,
                'status' => 'success',
                'file' => $file_name,
            );
        } else {
            //devolver el resultado 
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => "Error al subir file",
            );
        }

        return response()->json($data, $data['code']);
    }

    public function listar()
    {
        $politicas = Politica::all();
        if ($politicas) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Listado',
                'politicas' => $politicas
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay politicas registradas'
            );
        }

        return response()->json($data);
    }
    public function getBy($id)
    {
        $politica = Politica::where('id', $id)->first();
        if ($politica) {
            $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => 'Listado',
                'politica' => $politica
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay politica registrada'
            );
        }

        return response()->json($data, $data['code']);
    }
    public function getByName($name)
    {
        $politica = Politica::where('nombre', 'like', '%' . $name . '%')->get();
        if ($politica) {
            $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => 'Listado',
                'politicas' => $politica
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay politica registrada'
            );
        }

        return response()->json($data, $data['code']);
    }
    public function update($id, Request $request)
    {

        //Recoger datos del usuario por post
        /*$json = $request->input('json',null);
         $params_array = json_decode($json,true);*/
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);


        if (!empty($params_array)) {

            //Validar datos
            $validate = Validator::make($params_array, [
                'nombre' => 'required',
            ]);

            //Quitando campos que no se van a actualizar
            unset($params_array['id']);
            unset($params_array['created_at']);
            unset($params_array['updated_at']);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'El genero no se ha actualizado',
                    'errors' => $validate->errors()
                );
            } else {
                //Actualizar el genero en bd
                $politica_update = Politica::where('id', $id)->update($params_array);
                //Devolver el array con resultado 
                $data = array(
                    'code' => 200,
                    'status' => 'succes',
                    'politica' => $politica_update
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'El usuario no se ha identificado',
            );
        }

        return response()->json($data, $data['code']);
    }
    public function getPolitica($filename)
    {
        $isset = Storage::disk('politicas')->exists($filename);
        if ($isset) {
            $file = Storage::disk('politicas')->get($filename);
            $headers = array('Content-Type: application/pdf',);
            $filepath = "http://localhost:8000/storage/politicas/" . $filename;
            return FacadeResponse::download($filepath, 'filename.pdf', $headers);
        } else {
            $data = array(
                'code' => 404,
                'status' => 'error',
                'message' => 'el archivo no existe'
            );
            return response()->json($data, $data['code']);
        }
    }
    public function delete($id)
    {
        $politica = Politica::where('id', $id)->first();
        if ($politica) {
            $politica->delete();
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Politica Eliminada'
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No se ha eliminado la politica'
            );
        }
        return response()->json($data);
    }
}
