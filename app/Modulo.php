<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
    protected $table = "GLOBAL_TM_MODULOS";

    public function menus()
    {
        return $this->hasMany('App\Menu','FK_ID_MODULO');
    }
}
