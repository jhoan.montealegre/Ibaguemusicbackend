<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
    protected $table = "calificaciones";
    // protected $table = "MUSIC_TR_CALIFICACIONES";
    protected $primaryKey = "id";
    protected $fillable = ['calificacion', 'perfil_id', 'user_id'];
}
