<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table = "links";
    // protected $table = "music_tr_links";
    protected $primaryKey = "id";
    protected $fillable = ['nombre_cancion','plataforma','link','perfil_id'];
}
