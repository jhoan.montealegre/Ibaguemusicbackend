<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorito extends Model
{
    // protected $table = 'MUSIC_TR_FAVORITOS';
    protected $table = 'favoritos';
    protected $primaryKey = 'id';
    protected $fillable = ['perfil_id', 'user_id'];
}
