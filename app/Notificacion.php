<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
    protected $table = "notifications";
    // protected $table = "MUSIC_TM_NOTIFICACION";
    protected $fillable = ['id', 'nombre', 'mensaje'];
}
