<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credencial extends Model
{
    protected $table = 'credentials';
    // protected $table = 'GLOBAL_TM_CREDENCIALES';
    protected $connection = 'global';
    protected $primaryKey = 'LOGIN';
    protected $fillable = ['LOGIN', 'CLAVE', 'TOKEN'];
    public $timestamps = false;
    public $incrementing = false;




    public function usuario()
    {
        return $this->hasOne('App\Usuario');
    }
}
