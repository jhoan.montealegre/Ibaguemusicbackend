<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genero;
use Illuminate\Support\Facades\Validator;

class GeneroController extends Controller
{
    public function register(Request $request)
    {

        //Recoger datos del usuario por post
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);

        if (!empty($params) && !empty($params_array)) {
            //Validar datos
            $validate = Validator::make($params_array, [
                'nombre' => 'required',
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'El genero musical no se ha creado',
                    'errors' => $validate->errors()
                );
            } else {
                //validacion correcta
                //crear el genero
                $genero = new Genero();
                $genero->nombre = $params_array['nombre'];
                $genero->descripcion = $params_array['descripcion'];
                $genero->isborrado = 0;
                //guardar usuario
                $genero->save();
                $data = array(
                    'status' => 'succes',
                    'code' => 200,
                    'message' => 'El genero se ha creado',
                    'genero' => $genero
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'Los datos enviados no son correctos',
            );
        }

        return response()->json($data, $data['code']);
    }
    public function listar()
    {
        $generos = Genero::where('isborrado', 0)->get();
        if ($generos) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Listado',
                'generos' => $generos
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay generos registrados'
            );
        }

        return response()->json($data);
    }
    public function getBy($filtro, $valor)
    {
        if ($filtro == "id") {
            $generos = Genero::where($filtro, $valor)->where('isborrado', 0)->first();
        } else {
            $generos = Genero::where($filtro, 'like', '%' . $valor . '%')->where('isborrado', 0)->get();
        }
        if ($generos) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Listado',
                'genero' => $generos
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay generos registrados'
            );
        }

        return response()->json($data);
    }
    public function update($id, Request $request)
    {

        //Recoger datos del usuario por post
        /*$json = $request->input('json',null);
         $params_array = json_decode($json,true);*/
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);

        if (!empty($params_array)) {

            //Validar datos
            $validate = Validator::make($params_array, [
                'nombre' => 'required',
            ]);

            //Quitando campos que no se van a actualizar
            unset($params_array['id']);
            unset($params_array['created_at']);
            unset($params_array['updated_at']);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'El genero no se ha actualizado',
                    'errors' => $validate->errors()
                );
            } else {
                //Actualizar el genero en bd
                $genre_update = Genero::where('id', $id)->update($params_array);
                //Devolver el array con resultado 
                $data = array(
                    'code' => 200,
                    'status' => 'succes',
                    'genero' => $genre_update
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'El usuario no se ha identificado',
            );
        }

        return response()->json($data, $data['code']);
    }

    public function delete($id)
    {
        $genero = Genero::where('id', $id)->first();
        if ($genero) {
            $genero->isborrado = 1;
            $genero->save();
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Genero Eliminado'
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No se ha encontrado el genero'
            );
        }
        return response()->json($data);
    }
}
