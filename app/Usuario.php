<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'USUARIOS';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre', 'apellido', 'telefono','correo','contrasena','tipo_usuario'
    ];
    protected $hidden = [
        'contrasena'
    ];
}
