<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Foto;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FotoController extends Controller
{
    public function register(Request $request)
    {
        //Recoger datos del usuario por post
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);

        if (!empty($params) && !empty($params_array)) {
            //Validar datos
            $validate = Validator::make($params_array, [
                'perfil_id' => 'required',
                'foto' => 'required'
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'No se ha enviado el formulario correctamenter',
                    'errors' => $validate->errors()
                );
            } else {
                //validacion correcta
                //crear el genero
                $foto = new Foto();
                $foto->perfil_id = $params_array['perfil_id'];
                $foto->foto = $params_array['foto'];
                //guardar usuario
                $foto->save();
                $data = array(
                    'status' => 'succes',
                    'code' => 200,
                    'message' => 'El formulario ha sido enviado',
                    'foto' => $foto
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'Los datos enviados no son correctos',
            );
        }

        return response()->json($data, $data['code']);
    }
    public function upload(Request $request)
    {

        //Recoger datos por post
        $image = $request->file('foto');

        $validate = Validator::make($request->all(), [
            'perfil_id' => 'required|numeric',
            'foto' => 'image|mimes:jpg,jpeg,png,gif',
        ]);

        if ($validate->fails()) {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => $validate->errors(),
            );
        } else {
            if ($image) {
                $image_name = time() . $image->getClientOriginalName();
                Storage::disk('perfiles')->put($image_name, File::get($image));
                Foto::create([
                    'foto' => $image_name,
                    'perfil_id' => $request->perfil_id
                ]);
            }
            if ($image) {

                $data = array(
                    'code' => 200,
                    'status' => 'success',
                );
            } else {
                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'error al subir imagen'
                );
            }
        }
        return response()->json($data, $data['code']);
    }

    public function getByProfile($id)
    {
        $fotos = Foto::where('perfil_id', $id)->get();
        if ($fotos) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Listado',
                'fotos' => $fotos
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay fotos registradas'
            );
        }

        return response()->json($data);
    }

    public function delete($id)
    {
        $foto = Foto::where('id', $id)->first();
        if ($foto) {
            $foto->delete();
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Imagen Eliminada'
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No se ha encontrado la imagen'
            );
        }
        return response()->json($data);
    }
    public function getImage($filename)
    {
        $isset = Storage::disk('politicas')->exists($filename);
        if ($isset) {
            $file = $file = Storage::disk('politicas')->get($filename);
            return new Response($file, 200);
        } else {
            $data = array(
                'code' => 404,
                'status' => 'error',
                'message' => 'La imagen no existe'
            );
            return response()->json($data, $data['code']);
        }
    }
}
