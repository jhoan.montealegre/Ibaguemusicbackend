<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $table = "perfil";
    // protected $table = "MUSIC_TR_PERFIL";
    protected $primaryKey = "id";
    protected $fillable = ['nombre_real','nombre_artistico','num_integrantes','ciudad_origen','dispuesto_salir','representante','nombre_representante','telefono','correo','descripcion','pagina_web','genero','estado_perfil','tipo','usuario_id'];
    public function user(){
        return $this->belongsTo('App\Usuario');
    }
    public function fotos(){
        return $this->hasMany('App\Foto','perfil_id','id');
    }
    public function generos(){
        return $this->belongsTo('App\Genero','genero');
    }
    public function videos(){
        return $this->hasMany('App\Link','perfil_id','id')->where('plataforma','Youtube');
    }
    public function canciones(){
        return $this->hasMany('App\Link','perfil_id','id')->where('plataforma','SoundCloud');
    }
    public function favoritos(){
        return $this->hasMany('App\Favorito','perfil_id','id');
    }
}
