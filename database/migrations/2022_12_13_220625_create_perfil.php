<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfil', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_real')->nullable();
            $table->string('calificacion')->nullable();
            $table->string('nombre_artistico')->nullable();
            $table->string('num_integrantes')->nullable();
            $table->string('ciudad_origen')->nullable();
            $table->string('dispuesto_salir')->nullable();
            $table->string('representante')->nullable();
            $table->string('nombre_representante')->nullable();
            $table->string('telefono')->nullable();
            $table->string('correo')->nullable();
            $table->string('descripcion')->nullable();
            $table->string('pagina_web')->nullable();
            $table->string('genero')->nullable();
            $table->string('estado_perfil')->nullable();
            $table->string('tipo')->nullable();
            $table->string('usuario_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfil');
    }
}
