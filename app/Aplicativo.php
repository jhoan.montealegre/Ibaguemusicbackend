<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aplicativo extends Model
{
    protected $table = "GLOBAL_TM_APLICATIVOS";

    public function modulos()
    {
        return $this->hasMany('App\Modulo','FK_ID_APLICATIVO');
    }
}
