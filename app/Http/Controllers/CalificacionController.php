<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calificacion;
use App\Perfil;

class CalificacionController extends Controller
{
    public function calificar($perfil_id, $user_id, $calificacion)
    {
        $calificacion = Calificacion::create([
            'perfil_id' => $perfil_id,
            'calificacion' => $calificacion,
            'user_id' => $user_id,
        ]);



        $this->actualizarCalificacion($perfil_id);
        if ($calificacion) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => "Perfil Calificado, le has dado $calificacion->calificacion estrellas",
            );
        }
        return response()->json($data);
    }
    public function actualizarCalificacion($perfil)
    {
        $calificacion = $this->getCalificacion($perfil);
        $perfil = Perfil::where('id', $perfil)->first();
        $perfil->calificacion = $calificacion;
        $perfil->save();
    }
    public function getCalificacion($id)
    {
        $calificacion = Calificacion::where('perfil_id', $id)->sum('calificacion');
        $calificacionCant = Calificacion::where('perfil_id', $id)->count();
        $calificacion = $calificacion / $calificacionCant;
        $calificacion = round($calificacion, 0, PHP_ROUND_HALF_DOWN);
        return $calificacion;
    }
    public function listar()
    {
        $calificaciones = Calificacion::all();
        if ($calificaciones) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Listado',
                'calificaiones' => $calificaciones
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No han calificado este perfil'
            );
        }

        return response()->json($data);
    }
    public function delete($id)
    {
        $calificacion = Calificacion::where('id', $id)->first();
        if ($calificacion) {
            $calificacion->delete();
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'calificacion borrada',
            );
        }
        return response()->json($data);
    }
}
