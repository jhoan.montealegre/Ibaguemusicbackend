<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perfil;
use App\User;
use App\Calificacion;
use Illuminate\Support\Facades\Validator;

class PerfilController extends Controller
{
    public function register(Request $request)
    {

        //Recoger datos del usuario por post
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);

        if (!empty($params) && !empty($params_array)) {
            //Validar datos
            $validate = Validator::make($params_array, [
                'nombre_artistico' => 'required',
                'num_integrantes' => 'required|numeric',
                'ciudad_origen' => 'required',
                'dispuesto_salir' => 'required|numeric',
                'representante' => 'required',
                'telefono' => 'required',
                'correo' => 'required',
                'descripcion' => 'required',
                'genero' => 'required',
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'El perfil no se ha creado',
                    'errors' => $validate->errors()
                );
            } else {
                //validacion correcta
                //crear el perfil
                $perfil = new Perfil();
                $perfil->nombre_real = $params_array['nombre_real'];
                $perfil->nombre_artistico = $params_array['nombre_artistico'];
                $perfil->num_integrantes = $params_array['num_integrantes'];
                $perfil->ciudad_origen = $params_array['ciudad_origen'];
                $perfil->dispuesto_salir = $params_array['dispuesto_salir'];
                $perfil->representante = $params_array['representante'];
                $perfil->nombre_representante = $params_array['nombre_representante'];
                $perfil->telefono = $params_array['telefono'];
                $perfil->correo = $params_array['correo'];
                $perfil->descripcion = $params_array['descripcion'];
                if (isset($params_array['pagina_web'])) {
                    $perfil->pagina_web = $params_array['pagina_web'];
                }
                $perfil->genero = $params_array['genero'];
                $perfil->estado_perfil = $params_array['estado_perfil'];
                $perfil->tipo = $params_array['tipo'];
                $perfil->usuario_id = $params_array['usuario_id'];
                //guardar usuario
                $perfil->save();
                $data = array(
                    'status' => 'succes',
                    'code' => 200,
                    'message' => 'El perfil se ha creado',
                    'perfil' => $perfil
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'Los datos enviados no son correctos',
            );
        }

        return response()->json($data, $data['code']);
    }
    public function listar()
    {
        $perfiles = Perfil::all();
        if ($perfiles) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Listado',
                'perfiles' => $perfiles
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay perfiles activos'
            );
        }

        return response()->json($data);
    }
    public function getBy($filtro, $valor)
    {
        $perfiles = Perfil::with('fotos', 'generos', 'videos', 'canciones', 'favoritos')->where($filtro, 'like', '%' . $valor . '%')
            ->get();
        if ($perfiles) {
            $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => 'Listado',
                'perfiles' => $perfiles
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay pefiles activos'
            );
        }

        return response()->json($data);
    }
    public function update($id, Request $request)
    {
        //Recoger datos del usuario por post
        /*$json = $request->input('json',null);
         $params_array = json_decode($json,true);*/
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);

        if (!empty($params_array)) {

            //Validar datos
            $validate = Validator::make($params_array, [
                'nombre_artistico' => 'required',
                'num_integrantes' => 'required|numeric',
                'ciudad_origen' => 'required',
                'dispuesto_salir' => 'required|numeric',
                'representante' => 'required',
                'descripcion' => 'required',
                'genero' => 'required',
            ]);

            //Quitando campos que no se van a actualizar
            unset($params_array['id']);
            unset($params_array['usuario_id']);
            unset($params_array['estado_perfil']);
            unset($params_array['created_at']);
            unset($params_array['updated_at']);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'El perfil no se ha actualizado',
                    'errors' => $validate->errors()
                );
            } else {
                //Actualizar el genero en bd
                $perfil_update = Perfil::where('id', $id)->update($params_array);
                //Devolver el array con resultado 
                $data = array(
                    'code' => 200,
                    'status' => 'success',
                    'perfil' => $perfil_update
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'El usuario no se ha identificado',
            );
        }

        return response()->json($data, $data['code']);
    }

    public function delete($id)
    {
        $perfil = Perfil::where('id', $id)->first();
        if ($perfil) {
            $perfil->delete();
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Perfil Eliminado'
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No se ha encontrado el perfil'
            );
        }
        return response()->json($data);
    }

    public function calificar($perfil_id, $calificacion)
    {
        $user_id = 2;
        $calificacion = Calificacion::create([
            'perfil_id' => $perfil_id,
            'calificacion' => $calificacion,
            'user_id' => $user_id,
        ]);
        if ($calificacion) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => "Perfil Calificado, le has dado $calificacion->calificacion estrellas",
            );
        }
        return response()->json($data);
    }
    public function listarCalificacion()
    {
        $calificaciones = Calificacion::all();
        if ($calificaciones) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Listado',
                'calificaiones' => $calificaciones
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No han calificado este perfil'
            );
        }

        return response()->json($data);
    }
}
