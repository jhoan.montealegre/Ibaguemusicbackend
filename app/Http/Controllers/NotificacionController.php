<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notificacion;
use Illuminate\Support\Facades\Validator;

class NotificacionController extends Controller
{
    public function register(Request $request)
    {

        //Recoger datos del usuario por post
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);

        if (!empty($params) && !empty($params_array)) {
            //Validar datos
            $validate = Validator::make($params_array, [
                'nombre' => 'required',
                'mensaje' => 'required',
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'No se ha enviado el formulario correctamenter',
                    'errors' => $validate->errors()
                );
            } else {
                //validacion correcta
                //crear la notificacion
                $notificacion = new Notificacion();
                $notificacion->nombre = $params_array['nombre'];
                $notificacion->mensaje = $params_array['mensaje'];
                //guardar notificacion
                $notificacion->save();
                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'El formulario ha sido enviado',
                    'notificacion' => $notificacion
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'Los datos enviados no son correctos',
            );
        }

        return response()->json($data, $data['code']);
    }
    public function listar()
    {
        $notificaciones = Notificacion::all();
        if ($notificaciones) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Listado',
                'notificaciones' => $notificaciones
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay notificaciones registrados'
            );
        }

        return response()->json($data);
    }
    public function getBy($id)
    {
        $notificacion = Notificacion::where("id", $id)->first();
        if ($notificacion) {
            $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => 'Listado',
                'notificacion' => $notificacion
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay notificacion registrada'
            );
        }

        return response()->json($data);
    }
    public function getByName($name)
    {
        $notificacion = Notificacion::where('nombre', 'like', '%' . $name . '%')->get();
        if ($notificacion) {
            $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => 'Listado',
                'notificacion' => $notificacion
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay notificacion registrada'
            );
        }

        return response()->json($data);
    }
    public function update($id, Request $request)
    {

        //Recoger datos del usuario por post
        /*$json = $request->input('json',null);
         $params_array = json_decode($json,true);*/
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);


        if (!empty($params_array)) {

            //Validar datos
            $validate = Validator::make($params_array, [
                'nombre' => 'required',
                'mensaje' => 'required',
            ]);

            //Quitando campos que no se van a actualizar
            unset($params_array['id']);
            unset($params_array['created_at']);
            unset($params_array['updated_at']);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'la notificacion no se ha actualizado',
                    'errors' => $validate->errors()
                );
            } else {
                //Actualizar el genero en bd
                $notificacion_update = Notificacion::where('id', $id)->update($params_array);
                //Devolver el array con resultado 
                $data = array(
                    'code' => 200,
                    'status' => 'succes',
                    'notificacion' => $notificacion_update
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'El usuario no se ha identificado',
            );
        }

        return response()->json($data, $data['code']);
    }

    public function delete($id)
    {
        $notificacion = Notificacion::where('id', $id)->first();
        if ($notificacion) {
            $notificacion->delete();
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Notificacion Eliminado'
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No se ha encontrado la notificacion'
            );
        }
        return response()->json($data);
    }
}
