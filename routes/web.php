<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\AuthApi;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/api', function () {
    return response()->json(Storage::disk('politicas')->url('s1671069511Anllys Carolina Monsalve Lalinde.PDF'));
    // echo asset('storage/politicas/1671069511Anllys Carolina Monsalve Lalinde.PDF');
});

Route::post('api/users/login', 'UsuarioController@login');
Route::post('api/users/register', 'UsuarioController@register');
Route::get('prove', 'UsuarioController@prove');
Route::get('api/users/activar/{id}', 'UsuarioController@activarCuenta');

Route::get('api/users', 'UsuarioController@listar')->middleware(AuthApi::class);



Route::post('api/users/loginsocial', 'UsuarioController@loginSocial');
Route::post('api/users/registersocial', 'UsuarioController@registerSocial');
Route::get('api/users/recuperarcontrasena/{email}', 'UsuarioController@recuperarContrasena');
Route::put('api/users/update/{id}', 'UsuarioController@update')->middleware(AuthApi::class);
Route::put('api/users/updatepassword/{id}', 'UsuarioController@updatePassword');
Route::delete('api/users/delete/{id}', 'UsuarioController@delete')->middleware(AuthApi::class);
Route::get('api/users/{filtro}/{valor}', 'UsuarioController@getBy')->middleware(AuthApi::class);

Route::post('api/generos/register', 'GeneroController@register')->middleware(AuthApi::class);
Route::put('api/generos/update/{id}', 'GeneroController@update')->middleware(AuthApi::class);
Route::get('api/generos', 'GeneroController@listar')->middleware(AuthApi::class);
Route::delete('api/generos/delete/{id}', 'GeneroController@delete')->middleware(AuthApi::class);
Route::get('api/generos/{filtro}/{valor}', 'GeneroController@getBy')->middleware(AuthApi::class);

Route::post('api/notificaciones/register', 'NotificacionController@register')->middleware(AuthApi::class);
Route::put('api/notificaciones/update/{id}', 'NotificacionController@update')->middleware(AuthApi::class);
Route::get('api/notificaciones', 'NotificacionController@listar')->middleware(AuthApi::class);
Route::delete('api/notificaciones/delete/{id}', 'NotificacionController@delete')->middleware(AuthApi::class);
Route::get('api/notificaciones/{valor}', 'NotificacionController@getBy')->middleware(AuthApi::class);
Route::get('api/notificaciones/name/{valor}', 'NotificacionController@getByName')->middleware(AuthApi::class);


Route::post('api/perfiles/register', 'PerfilController@register')->middleware(AuthApi::class);
Route::put('api/perfiles/update/{id}', 'PerfilController@update')->middleware(AuthApi::class);
Route::get('api/perfiles', 'PerfilController@listar')->middleware(AuthApi::class);
Route::delete('api/perfiles/delete/{id}', 'PerfilController@delete')->middleware(AuthApi::class);
Route::get('api/perfiles/{filtro}/{valor}', 'PerfilController@getBy')->middleware(AuthApi::class);

Route::get('api/calificaciones/calificar/{perfilid}/{userid}/{calificacion}', 'CalificacionController@calificar')->middleware(AuthApi::class);
Route::get('api/calificaciones/listar', 'CalificacionController@listar')->middleware(AuthApi::class);
Route::get('api/calificaciones/calificacion/{id}', 'CalificacionController@getCalificacion')->middleware(AuthApi::class);
Route::delete('api/calificaciones/delete/{id}', 'CalificacionController@delete')->middleware(AuthApi::class);


Route::post('api/fotos/upload', 'FotoController@upload')->middleware(AuthApi::class);
Route::post('api/fotos/register', 'FotoController@register')->middleware(AuthApi::class);
Route::delete('api/fotos/delete/{id}', 'FotoController@delete')->middleware(AuthApi::class);
Route::get('api/fotos/perfil/{id}', 'FotoController@getByProfile')->middleware(AuthApi::class);
Route::get('api/fotos/{name}', 'FotoController@getImage');

Route::get('api/links', 'LinkController@listar')->middleware(AuthApi::class);
Route::get('api/links/name/{nombre}', 'LinkController@getByName')->middleware(AuthApi::class);
Route::post('api/links/register', 'LinkController@register')->middleware(AuthApi::class);
Route::put('api/links/update/{id}', 'LinkController@update')->middleware(AuthApi::class);
Route::delete('api/links/delete/{id}', 'LinkController@delete')->middleware(AuthApi::class);
Route::get('api/links/{id}', 'LinkController@getByProfile')->middleware(AuthApi::class);

Route::post('api/politicas/register', 'PoliticaController@register')->middleware(AuthApi::class);
Route::put('api/politicas/update/{id}', 'PoliticaController@update')->middleware(AuthApi::class);
Route::get('api/politicas', 'PoliticaController@listar')->middleware(AuthApi::class);
Route::get('api/politicas/{id}', 'PoliticaController@getBy')->middleware(AuthApi::class);
Route::delete('api/politicas/delete/{id}', 'PoliticaController@delete')->middleware(AuthApi::class);
Route::post('api/politicas/upload', 'PoliticaController@upload')->middleware(AuthApi::class);
Route::get('api/politicas/name/{name}', 'PoliticaController@getByName')->middleware(AuthApi::class);
Route::get('api/politicas/getPolitica/{name}', 'PoliticaController@getPolitica');

Route::get('api/favoritos/register/{perfil_id}/{user_id}', 'FavoritoController@register')->middleware(AuthApi::class);
Route::get('api/favoritos/{id}', 'FavoritoController@getByUser')->middleware(AuthApi::class);
Route::get('api/favoritos/isfav/{user_id}/{perfil_id}', 'FavoritoController@checkFav')->middleware(AuthApi::class);
