<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genero extends Model
{
    protected $table = "generos";
    // protected $table = "music_tm_generos";
    protected $primaryKey = "id";
    protected $fillable = ['nombre', 'descripcion', 'isborrado'];
}
