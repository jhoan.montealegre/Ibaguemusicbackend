<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soporte extends Model
{
    protected $table = "support";
    // protected $table = "MUSIC_TM_SOPORTES";
    protected $primaryKey = "id";
    protected $fillable = ['tema','solucion','user_id'];
}
