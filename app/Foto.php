<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    protected $table = "fotos";
    // protected $table = "MUSIC_TR_FOTOS";
    protected $primaryKey = "id";
    protected $fillable = ['foto','perfil_id'];

    public function perfiles(){
        return $this->belongsTo('App\Perfil','perfil_id');
    }
}
