<?php
namespace App\Helpers;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use App\Usuario;

class JwtAuth{
    public $key;
    public function __construct(){
        $this->key = 'clave_secreta_para_login_music_pisami';
    }

    public function signup($email, $password,$getToken = null){
        //buscar si existe el usuario 
        $user = Usuario::where([
            'correo' => $email, 
            'contrasena' => $password
        ])->first();
        //comprobar si son correctas
        $signup = false;
        if(is_object($user)){
            $signup = true;
        }

        //generar token con los datos del usuario 
        if($signup){
            $token = array(
                'sub' => $user->id,
                'email' => $user->correo,
                'name' => $user->nombre,
                'iat' => time(),
                'exp' => time() + (7 * 24 * 60 * 60 )
            );
            
            $jwt = JWT::encode($token,$this->key, 'HS256');
            $decode =  JWT::decode($jwt,$this->key, ['HS256']);
            //devolver datos decodificados o el token en funcion de un parametro
            if(is_null($getToken)){
                $data = $jwt;
            }
            else{
                $data = $decode;
            }
        }
        else{
            $data = array(
                'status' => 'error',
                'message' => 'Login incorrecto'
            );
        }
         
        return $data;
    }
    public function checkToken($jwt,$getIdentity = false)
    {
        $auth = false;
        try{
            $jwt = str_replace('""','',$jwt);
            $decoded =  JWT::decode($jwt,$this->key,['HS256']);
        }
        catch(\UnexpectedValueException $e){
            $auth = false;
        }
        catch(\DomainException $e){
            $auth = false;
        }

        if(!empty($decoded) && is_object($decoded) && isset($decoded->sub)){
            $auth = true;
        } 
        else{
            $auth = false;
        }

        if($getIdentity){
            return $decoded;
        }
        
        return $auth;

    }

}