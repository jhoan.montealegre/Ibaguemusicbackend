<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\link;
use Illuminate\Support\Facades\Validator;

class LinkController extends Controller
{
    public function register(Request $request)
    {

        //Recoger datos del usuario por post
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);

        if (!empty($params) && !empty($params_array)) {
            //Validar datos
            $validate = Validator::make($params_array, [
                'nombre_cancion' => 'required',
                'plataforma' => 'required',
                'link' => 'required',
                'perfil_id' => 'required'
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'El link no se ha registrado',
                    'errors' => $validate->errors()
                );
            } else {
                //validacion correcta
                //crear el genero
                $link = new Link();
                $link->nombre_cancion = $params_array['nombre_cancion'];
                $link->plataforma = $params_array['plataforma'];
                $link->link = $params_array['link'];
                $link->perfil_id = $params_array['perfil_id'];

                //guardar link
                $link->save();
                $data = array(
                    'status' => 'succes',
                    'code' => 200,
                    'message' => 'El link se ha registrado',
                    'link' => $link
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'Los datos enviados no son correctos',
            );
        }

        return response()->json($data, $data['code']);
    }
    public function listar()
    {
        $links = Link::all();
        if ($links) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Listado',
                'links' => $links
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay links registrados'
            );
        }

        return response()->json($data);
    }
    public function getByName($nombre)
    {
        $links = Link::where('nombre_cancion', 'like', '%' . $nombre . '%')->get();
        if ($links) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Listado',
                'links' => $links
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay links registradas'
            );
        }

        return response()->json($data);
    }
    public function getByProfile($id)
    {
        $links = Link::where('perfil_id', $id)->get();
        if ($links) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Listado',
                'links' => $links
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay links registradas'
            );
        }

        return response()->json($data);
    }

    public function update($id, Request $request)
    {
        //Recoger datos del usuario por post
        /*$json = $request->input('json',null);
         $params_array = json_decode($json,true);*/
        $json = $request->input();
        $params_array = $json;

        if (!empty($params_array)) {

            //Validar datos
            $validate = Validator::make($params_array, [
                'nombre_cancion' => 'required',
                'plataforma' => 'required',
                'link' => 'required',
                'perfil_id' => 'required'
            ]);

            //Quitando campos que no se van a actualizar
            unset($params_array['id']);
            unset($params_array['perfil_id']);
            unset($params_array['created_at']);
            unset($params_array['updated_at']);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'El link no se ha actualizado',
                    'errors' => $validate->errors()
                );
            } else {
                //Actualizar el link en bd
                $link = Link::where('id', $id)->update($params_array);
                //Devolver el array con resultado 
                $data = array(
                    'code' => 200,
                    'status' => 'succes',
                    'link' => $link
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'El usuario no se ha identificado',
            );
        }

        return response()->json($data, $data['code']);
    }

    public function delete($id)
    {
        $link = Link::where('id', $id)->first();
        if ($link) {
            $link->delete();
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Link Eliminado'
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No se ha encontrado el Link'
            );
        }
        return response()->json($data);
    }
}
