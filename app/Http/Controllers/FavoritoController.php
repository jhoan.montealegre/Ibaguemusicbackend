<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Favorito;

class FavoritoController extends Controller
{
    public function register($perfil_id,$user_id)
    {
        $favorito = Favorito::create([
            'perfil_id' => $perfil_id,
            'user_id' => $user_id
        ]);

        if($favorito){  
            $data = array(
                'code' => 200,
                'status' => 'success', 
            );
        }
        else{
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'error al agregarlo como favorito'           
             );
        }
    
        return response()->json($data,$data['code']);
    }
    public function getByUser($id)
    {
        $favoritos = Favorito::where('user_id',$id)->get();
        if($favoritos){  
            $data = array(
                'code' => 200,
                'status' => 'success', 
                'favoritos' => $favoritos,
            );
        }
        else{
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'error al agregarlo como favorito'           
             );
        }
        return response()->json($data,$data['code']);

    }
    public function deleteByUser($id)
    {
        $favorito = Favorito::where('user_id',$id)->delete();
        if($favorito){  
            $data = array(
                'code' => 200,
                'status' => 'success', 
            );
        }
        else{
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'error al agregarlo como favorito'           
             );
        }
    }

    public function checkFav($user_id,$perfil_id){
        $isfav = Favorito::where('user_id',$user_id)->where('perfil_id',$perfil_id)->first();
        if($isfav){
            $data = array(
                'status' => '1',           
             );
            return response()->json($data);
        }
        else{
            $data = array(
                'status' => '0',           
             );
            return response()->json($data);
        }
    }
}
