<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Genero;
use App\Credencial;
use App\Helpers\JwtAuth;
use App\Perfil;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\FacadesValidator;

class UsuarioController extends Controller
{
    public function register(Request $request)
    {


        //Recoger datos del usuario por post
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);

        if (!empty($params) && !empty($params_array)) {
            //Validar datos
            $validate = Validator::make($params_array, [
                'nombre' => 'required',
                'apellido' => 'required',
                'telefono' => 'required',
                'correo' => 'required|unique:usuarios',
                'contrasena' => 'required',
                'tipo_usuario' => 'required'

            ]);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'El usuario no se ha creado',
                    'errors' => $validate->errors()
                );
            } else {
                //validacion correcta
                //cifrar password
                $pwd = hash('sha256', $params->contrasena);
                //crear el usuario
                $user = new Usuario();
                $user->nombre = $params_array['nombre'];
                $user->apellido = $params_array['apellido'];
                $user->telefono = $params_array['telefono'];
                $user->correo = $params_array['correo'];
                // $user->email = $params_array['correo'];
                $user->contrasena = $pwd;
                $user->username = "$user->nombre.$user->apellido";
                $user->tipo_usuario = $params_array['tipo_usuario'];
                //guardar usuario
                $login = "$user->nombre.$user->apellido";
                $existe = Credencial::where('LOGIN', $login)->first();
                if ($existe) {
                    $existe->clave = $user->contrasena;
                    // $existe->password = Hash::make($user->contrasena);
                    $existe->save();
                } else {
                    $cr = array();
                    $cr['LOGIN'] = $login;
                    $cr['CLAVE'] = $user->contrasena;
                    $credencial = new Credencial($cr);
                    $credencial->save();
                    $user->save();
                    Perfil::create([
                        'usuario_id' => $user->id,
                    ]);
                    $this->correoActivacion($user->correo);
                }

                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'El usuario se ha creado',
                    'user' => $user
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'Los datos enviados no son correctos',
            );
        }

        return response()->json($data, $data['code']);
    }
    public function registerSocial(Request $request)
    {


        //Recoger datos del usuario por post
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);

        if (!empty($params) && !empty($params_array)) {
            //Validar datos
            $validate = Validator::make($params_array, [
                'nombre' => 'required',
                'apellido' => 'required',
                'correo' => 'required|unique:usuarios',
                'tipo_usuario' => 'required'

            ]);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'El usuario no se ha creado',
                    'errors' => $validate->errors()
                );
            } else {
                //validacion correcta
                //crear el usuario
                $user = new Usuario();
                $user->nombre = $params_array['nombre'];
                $user->apellido = $params_array['apellido'];
                $user->correo = $params_array['correo'];
                $user->confirmed = 1;
                $user->provider = $params_array['provider'];
                $user->provider_id = $params_array['provider_id'];
                $user->tipo_usuario = $params_array['tipo_usuario'];
                $user->username = "$user->nombre.$user->apellido";
                if ($params_array['tipo_usuario'] == 1) {
                    $user->estado = 0;
                } else {
                    $user->estado = 1;
                }
                //guardar usuario
                $login = "$user->nombre.$user->apellido";
                $existe = Credencial::where('LOGIN', $login)->first();
                if ($existe) {
                } else {
                    $cr = array();
                    $cr['LOGIN'] = $login;
                    $credencial = new Credencial($cr);
                    $credencial->save();
                    $user->save();
                    Perfil::create([
                        'usuario_id' => $user->id,
                    ]);
                }

                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'El usuario se ha creado',
                    'user' => $user
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'Los datos enviados no son correctos',
            );
        }

        return response()->json($data, $data['code']);
    }
    public function login(Request $request)
    {
        $jwtAuth = new JwtAuth();
        //recibir datos por post

        $json = $request->input('json', null);

        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);
        //Validar datos
        $validate = Validator::make($params_array, [
            'correo' => 'required|email',
            'contrasena' => 'required',
        ]);

        if ($validate->fails()) {
            //validacion fallo

            $signup = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'El usuario no se ha identificado',
                'errors' => $validate->errors()
            );
        } else {
            //validacion correcta
            //cifrar password
            $pwd = hash('sha256', $params->contrasena);
            $user = Usuario::where('correo', $params->correo)->first();
            $login = $user->username;
            $credencial = Credencial::where('LOGIN', $login)->where('CLAVE', $pwd)->first();
            if ($credencial) {
                //devolver token o datos
                $signup = $jwtAuth->signup($params->correo, $pwd);
                if (!empty($params->gettoken)) {
                    $signup = $jwtAuth->signup($params->correo, $pwd, true);
                }
                $credencial->token = $signup;
                $credencial->save();
                $credencial->nombre = $user->nombre;
                $credencial->apellido = $user->apellido;
                $credencial->telefono = $user->telefono;
                $credencial->correo = $user->correo;
                $credencial->userId = $user->id;
                $credencial->tipo_usuario = $user->tipo_usuario;
                $credencial->estado = $user->estado;
                $credencial->confirmed = $user->confirmed;
                return $credencial; // = Credencial::where('LOGIN',$login)->where('CLAVE',$pwd)->first();


            }
        }
        return response()->json($signup, 200);
    }

    public function prove()
    {
        $credencial = Usuario::where('correo', "mdgrisalez@misena.edu.co")->first();
        $credencial->correo = "mdgrisalez@misena.edu.co";
        $cr = $credencial->save();
        $data = array(
            'code' => 200,
            'status' => 'succes',
            'user' => $cr
        );
        return $data;
    }
    public function loginSocial(Request $request)
    {
        $jwtAuth = new JwtAuth();
        //recibir datos por post
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);
        //Validar datos
        $validate = Validator::make($params_array, [
            'id' => 'required',
        ]);

        if ($validate->fails()) {
            //validacion fallo

            $signup = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'El usuario no se ha identificado',
                'errors' => $validate->errors()
            );
        } else {
            //validacion correcta
            $user = Usuario::where('provider_id', $params->id)->first();
            $login = $user->username;
            $credencial = Credencial::where('LOGIN', $login)->first();
            if ($credencial) {
                $pwd = null;
                $params->correo = $user->correo;
                $signup = $jwtAuth->signup($params->correo, $pwd);
                if (!empty($params->gettoken)) {
                    $signup = $jwtAuth->signup($params->correo, $pwd, true);
                }
                $credencial->token = $signup;
                $credencial->save();
                $credencial->nombre = $user->nombre;
                $credencial->apellido = $user->apellido;
                $credencial->telefono = $user->telefono;
                $credencial->correo = $user->correo;
                $credencial->userId = $user->id;
                $credencial->tipo_usuario = $user->tipo_usuario;
                $credencial->estado = $user->estado;
                $credencial->confirmed = $user->confirmed;
                return $credencial; // = Credencial::where('LOGIN',$login)->where('CLAVE',$pwd)->first();


            }
        }
        return response()->json($signup, 200);
    }

    public function listar()
    {
        $users = Usuario::all();
        if ($users) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Listado',
                'user' => $users
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay usuarios registrados'
            );
        }

        return response()->json($data);
    }
    public function getBy($filtro, $valor)
    {
        if ($filtro == "id") {
            $users = Usuario::where($filtro, $valor)->first();
        } else {
            $users = Usuario::where($filtro, 'like', '%' . $valor . '%')->get();
        }
        if ($users) {
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Listado',
                'user' => $users
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No hay usuarios registrados'
            );
        }

        return response()->json($data);
    }
    public function update($id, Request $request)
    {
        //Recoger datos del usuario por post
        /*$json = $request->input('json',null);
         $params_array = json_decode($json,true);*/
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);

        if (!empty($params_array)) {

            //Validar datos
            $validate = Validator::make($params_array, [
                //'nombre' => 'required',
                //'apellido' => 'required',
                'telefono' => 'numeric',
                //'correo' => 'required',  
            ]);

            // $login = $params_array['login'];
            // $clave = $params_array['clave'];
            //Quitando campos que no se van a actualizar
            unset($params_array['id']);
            unset($params_array['contrasena']);
            unset($params_array['tipo_usuario']);
            unset($params_array['created_at']);
            unset($params_array['updated_at']);
            unset($params_array['login']);
            unset($params_array['clave']);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'El usuario no se ha actualizado',
                    'errors' => $validate->errors()
                );
            } else {
                //Actualizar el usuario en bd
                $user_update = Usuario::where('id', $id)->update($params_array);
                //Devolver el array con resultado 
                $data = array(
                    'code' => 200,
                    'status' => 'succes',
                    'user' => $user_update
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'El usuario no se ha identificado',
            );
        }

        return response()->json($data, $data['code']);
    }
    public function updatePassword($id, Request $request)
    {
        //Recoger datos del usuario por post
        /*$json = $request->input('json',null);
        $params_array = json_decode($json,true);*/
        $json = $request->input('json', null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json, true);

        if (!empty($params_array)) {

            //Validar datos
            $validate = Validator::make($params_array, [
                'correo' => 'required',
                'contrasena' => 'required',
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'La contrasena no ha sido cambiada',
                    'errors' => $validate->errors()
                );
            } else {
                //Actualizar el usuario en bd
                $user_update = Usuario::where('correo', $params_array['correo'])->first();
                $user_update->contrasena = hash('sha256', $params->contrasena);
                $user_update->save();
                $login = "$user_update->nombre.$user_update->apellido";
                $credencial = Credencial::where('LOGIN', $login)->first();
                $credencial->clave = hash('sha256', $params->contrasena);
                $credencial->save();
                //Devolver el array con resultado 
                $data = array(
                    'code' => 200,
                    'status' => 'succes',
                    'user' => $user_update
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'El usuario no se ha identificado',
            );
        }

        return response()->json($data, $data['code']);
    }
    public function delete($id)
    {
        $user = Usuario::where('id', $id)->first();
        if ($user) {
            $user->delete();
            $data = array(
                'status' => 'succes',
                'code' => 200,
                'message' => 'Usuario Eliminado'
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No se ha encontrado el usuario'
            );
        }
        return response()->json($data);
    }

    public function recuperarContrasena($correo)
    {
        $userExist = Usuario::where('correo', $correo)->first();
        if ($userExist) {
            $subject = "Recuperar Contrasena";
            $for = $correo;
            $cadena = $this->generarCodigo($correo);
            $content['cadena'] = $cadena;
            $mail = Mail::send('email', $content, function ($msj) use ($subject, $for) {
                $msj->from("ibaguemusicalapp@gmail.com", "Musica");
                $msj->subject($subject);
                $msj->to($for);
            });

            if (!Mail::failures()) {
                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Le hemos enviado un codigo a su correo electrónico para poder restablecer su contraseña',
                    'codigo' => $cadena
                );
            } else {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'No se ha enviado el correo',
                    'mail' => $mail,
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No se ha encontrado el correo'
            );
        }
        return response()->json($data);
    }
    public function correoActivacion($correo)
    {
        $userExist = Usuario::where('correo', $correo)->first();
        if ($userExist) {
            $subject = "Correo Activacion";
            $for = $correo;
            $content['user_id'] = $userExist->id;
            $mail = Mail::send('emailActivacion', $content, function ($msj) use ($subject, $for) {
                $msj->from("ibaguemusicalapp@gmail.com", "Musica");
                $msj->subject($subject);
                $msj->to($for);
            });

            if (!Mail::failures()) {
                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Le hemos enviado un correo para que active su cuenta',
                );
            } else {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'No se ha enviado el correo',
                    'mail' => $mail,
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'No se ha encontrado el correo'
            );
        }
        return response()->json($data);
    }
    public function activarCuenta($id)
    {
        $user = Usuario::find($id);
        if ($user->tipo_usuario == 1) {
            $user->confirmed = 1;
            $user->save();
        } else {
            $user->confirmed = 1;
            $user->estado = 1;
            $user->save();
        }
        return "Cuenta Confirmado";
    }
    function generarCodigo($cadena)
    {
        $key = '';
        $pattern = $cadena;
        $max = strlen($pattern) - 1;
        for ($i = 0; $i < 6; $i++) $key .= $pattern[mt_rand(0, $max)];
        return $key;
    }
}
