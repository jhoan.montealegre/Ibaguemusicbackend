<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Politica extends Model
{
    protected $table = "politicas";
    // protected $table = "MUSIC_TM_POLITICAS";
    protected $primaryKey = "id";
    protected $fillable = ['nombre','archivo'];
}
